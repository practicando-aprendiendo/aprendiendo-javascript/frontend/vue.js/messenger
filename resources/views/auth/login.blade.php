@extends('layouts.app')

@section('content')
<b-container>
  <!-- Content here -->
    <b-row align-h="center">
        <b-col cols="8">
            <b-card title="Inicio de sesión" class="my-3">

              @if($errors->any())
                <b-alert show variant="danger">
                  <ul class="mb-0">
                    @foreach($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </b-alert>
              @else
                <b-alert show>
                  Por favor ingresa tus datos:
                </b-alert>
              @endif


              <b-form method="POST" action="{{ route('login') }}">
                  {{ csrf_field() }}
                <b-form-group
                  id="exampleInputGroup1"
                  label="Correo electronico:"
                  label-for="email"
                >
                  <b-form-input
                    id="email"
                    name="email"
                    type="email"
                    value="{{ old('email') }}" autofocus required
                    placeholder="Coloca aquí tú correo 'jhondoe@email.com'"
                  />
                </b-form-group>

                <b-form-group
                  id="exampleInputGroup1"
                  label="Contraseña:"
                  label-for="password"
                >
                  <b-form-input
                    id="password"
                    name="password"
                    type="password"
                    required
                  />
                </b-form-group>

                <b-form-group>
                  <b-form-checkbox
                    name="remember"
                    {{ old('remember') ? 'checked="true"' : '' }}
                  >
                    Recordar sesión
                  </b-form-checkbox>
                </b-form-group>

                <b-button
                  type="submit"
                  variant="primary">
                  Iniciar sesión
                </b-button>

                <b-button
                  href="{{ route('password.request') }}"
                  variant="link">
                  ¿Olvidaste tú contraseña?
                </b-button>
              </b-form>
          </b-card>
        </b-col>
    </b-row>
  <!-- Content here -->
<b-container>
@endsection
